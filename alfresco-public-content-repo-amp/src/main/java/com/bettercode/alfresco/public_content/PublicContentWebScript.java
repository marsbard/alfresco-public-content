package com.bettercode.alfresco.public_content;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.activiti.engine.impl.util.json.JSONObject;
import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.Cache;
import org.springframework.extensions.webscripts.DeclarativeWebScript;
import org.springframework.extensions.webscripts.Status;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;

public class PublicContentWebScript extends AbstractWebScript {

	public PublicContentWebScript(){
		System.out.println("PublicContentWebScript CONSTRUCTED");
	}
	
	@Override
	public void execute(WebScriptRequest req, WebScriptResponse res) throws IOException {
    	// build a json object
    	JSONObject obj = new JSONObject();
    	
    	// put some data on it
    	obj.put("ping", "ok");
    	
    	// build a JSON string and send it back
    	String jsonString = obj.toString();
    	res.getWriter().write(jsonString);		
	}
//    protected Map<String, Object> executeImpl(
//            WebScriptRequest req, Status status, Cache cache) {
//    	
//        Map<String, Object> model = new HashMap<String, Object>();
//        model.put("fromJava", req.getParameterNames());
//        return model;
//    }
}
