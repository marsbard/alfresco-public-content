package com.bettercode.alfresco.public_content.integration;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.MalformedURLException;

import org.json.JSONException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.*;
import static com.jayway.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

public class PublicContentWebScriptTest {

	private static final String USER_ADMIN = "admin";
	private static final String LOCAL_URL = "http://localhost:8080/alfresco/s/public";
//	private static final String LOCAL_URL = "http://localhost:8080/alfresco/";
	private String url;

	@Before
	public void setUp() throws Exception {
		System.out.println("===========================");
		url = LOCAL_URL;
	}

	@After
	public void tearDown() {
		System.out.println("**********");
		
	}
	
	@Test
	public void obviouslyFailingTest() throws MalformedURLException, IOException, JSONException {
		System.out.println("obviouslyFailingTest()");
		get(url).then().body("ping", equalTo("ok"));

	}
	
	@Test
	public void testThatDoesNotTestJustLogs(){
		System.out.println("testThatDoesNotTestJustLogs()");

		System.out.println("url  : " + url);
		System.out.println("body:");
		get(url).then().log().body();
		System.out.println("status:");
		get(url).then().log().status(); // Only log the status line
		System.out.println("response headers:");
		get(url).then().log().headers();  // Only log the response headers
		System.out.println("response cookies:");
		get(url).then().log().cookies();  // Only log the response cookies
	}

}
